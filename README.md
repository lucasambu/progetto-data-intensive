# Progetto di Programmazione di Applicazioni Data Intensive

**Anno accademico 2018/2019**

Di: 

* Sambuchi Luca
* Galdenzi Federico
* Nardini Filippo

[Dataset](https://www.kaggle.com/mohansacharya/graduate-admissions) utilizzato.
 
[Link](https://colab.research.google.com/drive/1sywCXjPLFtB1nlkrZyBafmSTj43aXRis) per l'esecuzione in Colab.

